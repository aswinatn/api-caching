
> npm install

Run the application
> node server

Open http://localhost:3000 in a browser

Hit the "Call REST" button
Based on the max-age setup when you will hit the "Call REST"
      Browser will refresh by fetching new counter value
      *or* Get the data from the browser cache

In understand
1. Change the MAX_AGE in server.js to different values, keep in mind value is in seconds
2. Restart the server
3. Hit the "Call REST" on browser

#currently facing issue